﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BXJG.Utils.File
{
    public class FileDto
    {
        public string FilePath { get; set; }
        public string ThumPath { get; set; }

        public string FileUrl { get; set; }
        public string ThumUrl { get; set; }
    }
}
