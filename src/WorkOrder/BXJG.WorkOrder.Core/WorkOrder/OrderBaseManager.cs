﻿using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.Timing;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BXJG.WorkOrder.WorkOrder
{
    public class WorkOrderCreateDtoBase
    { 
        public long CategoryId { get; set; }
        public UrgencyDegree UrgencyDegree { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTimeOffset? Time { get; set; }
        public DateTimeOffset? EstimatedExecutionTime { get; set; }
        public DateTimeOffset? EstimatedCompletionTime { get; set; }
    }

    public class WorkOrderCreateDto : WorkOrderCreateDtoBase {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string ExtendedField1 { get; set; }
        public string ExtendedField2 { get; set; }
        public string ExtendedField3 { get; set; }
        public string ExtendedField4 { get; set; }
        public string ExtendedField5 { get; set; }
    }
    public abstract class OrderBaseManager<TEntity,TDto> : DomainServiceBase 
        where TEntity : OrderBaseEntity
        where TDto:WorkOrderCreateDtoBase
    {
        protected readonly IRepository<TEntity, long> repository;

        protected OrderBaseManager(IRepository<TEntity, long> repository)
        {
            this.repository = repository;
        }
        //分类 紧急程度可以定义参数默认值，进一步获取设置系统的默认值
        //工单的创建场景有：后台管理员创建、客户提交、某些事件如销售订单产生时自动创建，这些场景通常对应应用层方法或事件处理程序，它们都调用此方法

        public async Task<TEntity> CreateAsync(TDto dto)
        {
            //其它逻辑，暂时忽略
            if (!dto.Time.HasValue)
                dto.Time = Clock.Now;
            var entity = Create(dto);
            await repository.InsertAsync(entity);
            //await CurrentUnitOfWork.SaveChangesAsync();//保存以更新id为自增id
            return entity;
        }
        /// <summary>
        /// 子类重写，调用构造函数new一个工单
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="urgencyDegree"></param>
        /// <param name="title"></param>
        /// <param name="time"></param>
        /// <param name="description"></param>
        /// <param name="estimatedExecutionTime"></param>
        /// <param name="estimatedCompletionTime"></param>
        /// <param name="extendedField1"></param>
        /// <param name="extendedField2"></param>
        /// <param name="extendedField3"></param>
        /// <param name="extendedField4"></param>
        /// <param name="extendedField5"></param>
        /// <returns></returns>
        protected abstract TEntity Create(TDto dto);

        public virtual Task DeleteAsync(TEntity entity)
        {
            if (entity.Status != Status.ToBeConfirmed)
                throw new UserFriendlyException("此状态的工单不允许删除！");

            return repository.DeleteAsync(entity);
        }
    }

    public class OrderManager : OrderBaseManager<OrderEntity,WorkOrderCreateDto>
    {
        public OrderManager(IRepository<OrderEntity, long> repository) : base(repository)
        {
        }

        protected override OrderEntity Create(WorkOrderCreateDto dto)
        {
            return new OrderEntity(dto.CategoryId,
                                   dto.UrgencyDegree,
                                   dto.Title,
                                   dto.Time.Value,
                                   dto.Description,
                                   dto.EstimatedExecutionTime,
                                   dto.EstimatedCompletionTime,
                                   dto.EntityType,
                                   dto.EntityId,
                                   dto.ExtendedField1,
                                   dto.ExtendedField2,
                                   dto.ExtendedField3,
                                   dto.ExtendedField4,
                                   dto.ExtendedField5);
        }
    }
}
