﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BXJG.WorkOrder.Employee
{
    public class EmployeeDto
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }
    }
}
